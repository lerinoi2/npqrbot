<?php

class Def {
  const BASE_URL = 'https://api.telegram.org/bot';

  protected $curl;
  protected $token;
  protected $defKeyboard;

  public function __construct($token = false) {
      $this->token = $token;
      $this->curl = curl_init();
  }

  public function __destruct() {
      $this->curl && curl_close($this->curl);
  }

  public function getUrl() {
      return self::BASE_URL.$this->token;
  }

  public function log($arg) {
    if ($arg != false) {
      return '<pre>'.print_r($arg,true).'</pre>';
    } else {
      return 'false';
    }
  }

  public function sendDefaultKeyboard($chatId, $text, $parseMode = 'HTML',
                                      $keyboardMode = false, $disablePreview = false,
                                      $replyToMessageId = null, $disableNotification = false)
  {
      $data = [
          'chat_id' => $chatId,
          'text' => $text,
          'reply_markup' => $this->getKeyboard($this->defKeyboard, $keyboardMode),
          'parse_mode' => $parseMode,
          'disable_web_page_preview' => $disablePreview,
          'reply_to_message_id' => (int)$replyToMessageId,
          'disable_notification' => (bool)$disableNotification,
      ];
      return $this->call('sendMessage', $data);
  }

  public function call($method, array $data = null) {

    $options = [
        CURLOPT_URL => $this->getUrl().'/'.$method,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_POST => true,
        CURLOPT_POSTFIELDS => $data,
        CURLOPT_HEADER => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_ENCODING => "",
        CURLOPT_CONNECTTIMEOUT => 10,
        CURLOPT_COOKIESESSION => false,
        CURLOPT_AUTOREFERER => 1,
        CURLOPT_TIMEOUT => 20,
        CURLOPT_MAXREDIRS => 5,
        CURLOPT_FAILONERROR => false,
//        CURLOPT_PROXY => "18.188.16.18:1080",
//        CURLOPT_PROXYTYPE => CURLPROXY_SOCKS5,
//        CURLOPT_PROXYUSERPWD => "ckdigital:xrPkjtAQDU*n@5bn",
    ];
    curl_setopt_array($this->curl, $options);
    $res = curl_exec($this->curl);
    return $res==false?false:json_decode($res, true);
  }

  public function getKeyboard($keyboard = null, $mode = false)
  {
    if (is_null($keyboard)) {
      return null;
    }
    if ($mode == 'inline') {
      return json_encode(['inline_keyboard' => $keyboard]);
    } else {
      return json_encode(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
    }
  }

  public function setWebhook($url = '', $certificate = null) {
      return $this->call('setWebhook', ['url' => $url, 'certificate' => $certificate]);
  }

  public function sendMessage($chatId, $text, $keyboard = null, $parseMode = null,
                              $keyboardMode = false, $disablePreview = false,
                              $replyToMessageId = null, $disableNotification = false) {
    $data = [
      'chat_id' => $chatId,
      'text' => $text,
      'reply_markup' => $this->getKeyboard($keyboard, $keyboardMode),
      'parse_mode' => $parseMode,
      'disable_web_page_preview' => $disablePreview,
      'reply_to_message_id' => (int)$replyToMessageId,
      'disable_notification' => (bool)$disableNotification,
    ];

    return $this->call('sendMessage', $data);
  }

  public function sendPhoto($chatId, $photo, $keyboard = null, $caption = null,
                            $replyToMessageId = null, $disableNotification = false) {
    $data = [
      'chat_id' => $chatId,
      'photo' => $photo,
      'reply_markup' => $this->getKeyboard($keyboard),
      'caption' => $caption,
      'reply_to_message_id' => $replyToMessageId,
      'disable_notification' => (bool)$disableNotification,
    ];

    return $this->call('sendPhoto', $data);
  }

    public function getFile($file_id, $file_size) {
      $data = [
        'file_id' => $file_id,
        'file_size' => $file_size,
      ];
      return $this->call('getFile', $data);
    }

  public function sendSticker($chatId, $sticker, $keyboard = null,
                              $replyToMessageId = null, $disableNotification = false) {
    $data = [
      'chat_id' => $chatId,
      'sticker' => $sticker,
      'reply_to_message_id' => $replyToMessageId,
      'reply_markup' => $this->getKeyboard($keyboard),
      'disable_notification' => (bool)$disableNotification,
    ];

    return $this->call('sendSticker', $data);
  }

  public function editMessageText($chatId, $messageId, $text, $keyboard = null,
                                  $parseMode = null, $keyboardMode = false, $disablePreview = false) {
    $data = [
      'chat_id' => $chatId,
      'message_id' => $messageId,
      'text' => $text,
      'reply_markup' => $this->getKeyboard($keyboard, $keyboardMode),
      'parse_mode' => $parseMode,
      'disable_web_page_preview' => $disablePreview,
    ];

    return $this->call('editMessageText', $data);
  }

  public function editMessageReplyMarkup($chatId, $messageId, $keyboard = null, $keyboardMode = false) {
    $data = [
      'chat_id' => $chatId,
      'message_id' => $messageId,
      'reply_markup' => $this->getKeyboard($keyboard, $keyboardMode),
    ];

    return $this->call('editMessageReplyMarkup', $data);
  }

  public function answerCallbackQuery($callbackQueryId, $text = null, $showAlert = false) {
    $data = [
      'callback_query_id' => $callbackQueryId,
      'text' => $text,
      'show_alert' => (bool)$showAlert,
    ];
    return $this->call('answerCallbackQuery', $data);
  }

  public function deleteMessage($chatId, $messageId) {
    $data = [
      'chat_id' => $chatId,
      'message_id' => $messageId
    ];
    return $this->call('deleteMessage', $data);
  }

  function declOfNum($number, $titles) {
    $cases = array (2, 0, 1, 1, 1, 2);
    return $titles[ ($number%100 > 4 && $number %100 < 20) ? 2 : $cases[min($number%10, 5)] ];
  }

  public function getEmoji($byteCode) {
    return hex2bin(str_replace('\x','',$byteCode));
  }

  public function track($message, $eventName = 'Message')
  {
    if ($this->tracker instanceof Botan && $message) {
      return $this->tracker->track($message, $eventName);
    }
  }
}

?>