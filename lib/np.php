<?php
include_once 'default.php';
include_once 'locale.php';

use PHPZxing\PHPZxingDecoder;

class Bot extends Def
{

    private $host = 'vip15.hosting.reg.ru:3306';
    private $dbname = 'u0292935_zakaz';
    private $login = 'u0292935_zakaz';
    private $password = 'ckdigital';
    private $charset = 'utf8';
    private $connection;

    public function saveFile($file_path)
    {
        $url = 'https://api.telegram.org/file/bot856327500:AAGXof_6PNaKVVoe6LO1Lpu-J0dT4N4dkWQ/' . $file_path;
        $file = 'img/' . time() . '.jpg';
        $dest_file = fopen($file, 'wb');
        $resource = curl_init();
        curl_setopt($resource, CURLOPT_URL, $url);
        curl_setopt($resource, CURLOPT_FILE, $dest_file);
        curl_setopt($resource, CURLOPT_HEADER, 0);
        curl_exec($resource);
        curl_close($resource);
        fclose($dest_file);
        return $this->decodeImage($file);
    }

    public function decodeImage($path)
    {
        $decoder = new PHPZxingDecoder();
        $data = $decoder->decode('/var/www/ba-webdev.ru/npqrbot/' . $path);
        if ($data->isFound()) {
            $tmp = explode(' ', utf8_decode($data->getImageValue()));
            $new_array = array_values(array_filter($tmp, static function ($element) {
                return !empty($element);
            }));
            $camp = implode(' ', array_slice($new_array, 4));
            $date = strtotime($new_array[3]);
            $name = $new_array[0] . ' ' . $new_array[1] . ' ' . $new_array[2];
            $info = $this->getInfo($date, $name, $camp);
            $result = [
                'ФИО' => $name,
                'Дата рождения' => $new_array[3],
                'Смена' => $camp,
            ];
            unlink('/var/www/ba-webdev.ru/npqrbot/' . $path);

            return array_merge($result, $info);
        }
        else {
            $taille_echant = 70;
            $url_img = '/var/www/ba-webdev.ru/npqrbot/' . $path;
            $part = pathinfo($url_img);
            $ext = strtolower($part['extension']);
            if ($ext == "jpg" or $ext == "jpeg") $img = imagecreatefromjpeg($url_img);
            if ($ext == "gif") $img = imagecreatefromgif($url_img);
            if ($ext == "png") $img = imagecreatefrompng($url_img);
            list($x, $y) = getimagesize($url_img);
            $img3 = imagecreatetruecolor($taille_echant, $taille_echant);
            imagecopyresampled($img3, $img, 0, 0, 0, 0, $taille_echant, $taille_echant, $x, $y);
            imagefilter($img, IMG_FILTER_BRIGHTNESS, 40);
            imagefilter($img, IMG_FILTER_CONTRAST, -40);
            imagealphablending($img, true);
            imagejpeg($img, '/var/www/ba-webdev.ru/npqrbot/img/new_img.jpg');
            imagedestroy($img);

            $data = $decoder->decode('/var/www/ba-webdev.ru/npqrbot/img/new_img.jpg');
            if ($data->isFound()) {
                $tmp = explode(' ', utf8_decode($data->getImageValue()));
                $new_array = array_values(array_filter($tmp, static function ($element) {
                    return !empty($element);
                }));
                $camp = implode(' ', array_slice($new_array, 4));
                $date = strtotime($new_array[3]);
                $name = $new_array[0] . ' ' . $new_array[1] . ' ' . $new_array[2];
                $info = $this->getInfo($date, $name, $camp);
                $result = [
                    'ФИО' => $name,
                    'Дата рождения' => $new_array[3],
                    'Смена' => $camp,
                ];
                unlink('/var/www/ba-webdev.ru/npqrbot/' . $path);
                unlink('/var/www/ba-webdev.ru/npqrbot/img/new_img.jpg');

                return array_merge($result, $info);
            }
            else {
                unlink('/var/www/ba-webdev.ru/npqrbot/img/new_img.jpg');
                unlink('/var/www/ba-webdev.ru/npqrbot/' . $path);

                return 'Не удалось распознать';
            }
        }

    }

    public function getInfo($date, $name, $camp)
    {
        $order_ids = $this->getData("select `id`, `squad`, `room`, `order_id` from `np_vouchers` where name like \"%" . $name . "%\" and birth = '" . $date . "'");
        $camp = $this->getData("select `id` from `np_camps_shift` where name like \"" . $camp . "\"")[0]['id'];
        $count = count($order_ids);
        if ($count > 1) {
            foreach ($order_ids as $val) {
                $qwe[] = $val["order_id"];
            }
            $q = 'in (' . implode(',', $qwe) . ')';
        }
        else {
            $q = "= '" . $order_ids[0]['order_id'] . "'";
        }

        $result = array();
        $order = $this->getData("select id from `np_camp_order` where `camp_shift_id` = '" . $camp . "' and id " . $q)[0]["id"];
        foreach ($order_ids as $id) {
            if ($id['order_id'] === $order) {
                $result = $id;
            }
        }
        $squadId = $result['squad'];
        $squad = $this->getData("select `number`, `home` from np_squad where id = '" . $squadId . "'")[0];
        $home = $this->getData("select `home` from np_home where id = '" . $squad['home'] . "'")[0]["home"];
        $room = $this->getData("select `number` from np_room where id = '" . $result['room'] . "'")[0]["number"];

        $res = [
            'Отряд' => $squad['number'] ?: 'Не задано',
            'Корпус' => $home ?: 'Не задано',
            'Комната' => $room ?: 'Не задано',
        ];

        return $res;
    }

    public function getData($query)
    {
        $this->connection = "mysql:host=$this->host;dbname=$this->dbname;charset=$this->charset";
        try {
            $db = new PDO($this->connection, $this->login, $this->password);
        }
        catch (PDOException $e) {
            echo $e->getMessage();
        }
        $stmt = $db->prepare($query);
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $out[] = $row;
        }
        return $out ? $out : false;
    }

}
