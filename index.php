<?php
include_once 'config.php';
$bot = new Bot($botToken);

$response = json_decode(file_get_contents('php://input'), TRUE);

if ($response['message']) {
    $message = $response['message'];
    $chatId = $message['chat']['id'];
    $text = $message['text'];
}
else {
    $message = $response['callback_query']['message'];
    $chatId = $message['chat']['id'];
    $data = json_decode($response['callback_query']['data']);
}

try {
    switch ($text) {
        case '/start':
            $name = $response['message']['chat']['first_name'] . ' ' . $response['message']['chat']['last_name'];
            $keybord = [[$l['scan']]];
            $bot->sendMessage($chatId, $name . "\n" . $l['start'], $keybord);
            break;

        case $l['scan']:
            $bot->sendMessage($chatId, $l['scanText']);
            break;

        default:
            if ($message['photo']) {
                $file = array_pop($message['photo']);
                $res = $bot->getFile($file['file_id']);
                $bot->sendMessage($chatId, 'Файл успешно загружен');
                $qwe = $bot->saveFile($res['result']['file_path']);
                if (is_array($qwe)) {
                    foreach ($qwe as $key => $value) {
                        $bot->sendMessage($chatId, $key . ' - ' . print_r($value, true));
                    }
                } else {
                    $bot->sendMessage($chatId, $qwe);
                }

            }
            break;
    }
}
catch (Exception $ex) {
    echo $ex->getMessage();
}